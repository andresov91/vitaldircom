require 'spreadsheet'

class Company < ApplicationRecord
  belongs_to :country
  has_and_belongs_to_many :activity_types
  has_and_belongs_to_many :product_categories
  has_many :contacts

  def self.import(file)
    num_companies_imported = 0
    
  	Spreadsheet.client_encoding = 'UTF-8'
  	book = Spreadsheet.open file.path
  	sheet1 = book.worksheet 0
  	sheet1.each 1 do |row|
  	  company = Company.find_by(ruc: row[1])
      activity_ids = []
      category_ids = []

  	  if !company
  	  	country = Country.find_by(name: row[6].strip.capitalize)
        
        if row[4]
          arr_activities = row[4].strip.split(',')
          arr_activities.each do |activity|
            obj_activity = ActivityType.find_by(name: activity.capitalize)
            activity_ids << obj_activity.id
          end
        end

        if row[5]
          arr_categories = row[5].strip.split(',')
          arr_categories.each do |category|
            obj_category = ProductCategory.find_by(name: category.capitalize)
            category_ids << obj_category.id
          end
        end

        company_created = Company.create(ruc: row[1].to_s.strip, name: row[2], short_name: row[3], country_id: country.id, activity_type_ids: activity_ids, product_category_ids: category_ids)
        if company_created
          num_companies_imported += 1
        end
  	  end
    end
    return num_companies_imported
  end

  def self.export(country_id,activity_type_id,product_category_id)
    xls_report = StringIO.new

    Spreadsheet.client_encoding = 'UTF-8'
    book = Spreadsheet::Workbook.new
    sheet1 = book.create_worksheet

    sheet1.row(0).push "Ruc", "Nombre Empresa", "Nombre Corto", "Nombre Comercial", "Direccion", "Email", "Website", "Fundacion", "Tipo Actividad", "Linea Producto", "Pais", "Contacto", "Posicion", "Telefono Fijo", "Telefono Movil", "Email", "Contacto para"
    i = 1;

    companies = Company
                .select("companies.ruc, companies.name as c_name, companies.short_name, companies.trade_name, companies.address, companies.email, companies.website, companies.foundation, companies.country_id, activity_types.name as at_name, product_categories.name as pc_name, contacts.id as co_id, contacts.name as co_name, contacts.position, contacts.phone, contacts.cellphone, contacts.email as co_email")
                .joins(:activity_types, :product_categories)
                .left_outer_joins(:contacts)
                .where("companies.country_id = ? AND activity_types.id = ? AND product_categories.id = ?", country_id, activity_type_id, product_category_id)
    companies.each do |company|
      # Obtener pais
      country = Country.find(company.country_id)
      # Obtener contacto
      if company.co_id
        arr_contact_types = []
        contact_types = Contact.find(company.co_id).contact_types
        contact_types.each do |contact_type|
          arr_contact_types << contact_type.name
        end
        str_contact_types = arr_contact_types.join(",")
      end
      sheet1.row(i).push company.ruc, company.c_name, company.short_name, company.trade_name, company.address, company.email, company.website, company.foundation, company.at_name, company.pc_name, country.name, company.co_name, company.position, company.phone, company.cellphone, company.co_email, str_contact_types
      i += 1
    end

    book.write(xls_report)
    xls_report.string

  end

end
