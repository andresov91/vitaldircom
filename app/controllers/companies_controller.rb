class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  # GET /companies.json
  def index
    # @companies = Company.where(country_id: 1).order(created_at: :desc).page(params[:page].to_i).per(20)    
  end

  # GET /companies/search
  def search   
    if company_search_param.present?
      @companies = Company.where("country_id = ? AND name ILIKE ?", params[:company][:country_id],"%#{params[:search]}%").order(name: :asc).page(params[:page].to_i).per(20) 
    else
      @companies = Company.where(country_id: params[:company][:country_id]).order(name: :asc).page(params[:page].to_i).per(20) 
    end
  end

  def import
    num_companies_imported = Company.import(params[:file])
    if num_companies_imported > 0
      redirect_to companies_url, notice: "#{num_companies_imported} empresas importadas exitosamente !"
    else
      redirect_to companies_url, notice: "#{num_companies_imported} empresas importadas. Todas las empresas ya existían previamente."
    end
  end

  def export
    if request.post?
      data = Company.export(params[:company][:country_id],params[:company][:activity_type_id],params[:company][:product_category_id])
      send_data(data, :type => "text/xls;charset=utf-8;header=present", :filename => "Reporte.xls")
      #redirect_to companies_url
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'La empresa fue creada exitosamente.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'La empresa fue editada exitosamente.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'La empresa fue eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:ruc, :name, :short_name, :trade_name, :address, :email, :website, :foundation, :country_id, activity_type_ids:[], product_category_ids:[])
    end

  def company_search_param
    params[:search] || {}
  end

  def company_country_param
    params[:company][:country_id] || {}
  end
end
