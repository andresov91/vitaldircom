class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @company = Company.find(params[:company_id])
    @contacts = Company.find(params[:company_id]).contacts
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    #company = Company.find(params[:company_id])
    #@contact = company.contacts.find(params[:id])
  end

  # GET /contacts/new
  def new
    @company = Company.find(params[:company_id])
    @contact = Company.find(params[:company_id]).contacts.build
    
    #@contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
    @company = Company.find(params[:company_id])
    @contact = Company.find(params[:company_id]).contacts.find(params[:id])
    #@contact = company.contacts.find(params[:id])
  end

  # POST /contacts
  # POST /contacts.json
  def create
    company = Company.find(params[:company_id])
    @contact = company.contacts.create(contact_params)
    #@contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to [@contact.company, @contact], notice: 'El contacto fue creado exitosamente.' }
        format.json { render :show, status: :created, location: [@contact.company, @contact] }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    company = Company.find(params[:company_id])
    @contact = company.contacts.find(params[:id])

    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to [@contact.company, @contact], notice: 'El contacto fue editado exitosamente.' }
        format.json { render :show, status: :ok, location: [@contact.company, @contact] }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    company = Company.find(params[:company_id])
    @contact = company.contacts.find(params[:id])
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to company_contacts_url, notice: 'El contacto fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:name, :position, :phone, :cellphone, :email, :other_email, :company_id, contact_type_ids:[])
    end
end
