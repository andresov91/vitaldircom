json.extract! company, :id, :ruc, :name, :short_name, :trade_name, :address, :email, :website, :foundation, :country_id, :created_at, :updated_at
json.url company_url(company, format: :json)