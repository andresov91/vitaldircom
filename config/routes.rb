Rails.application.routes.draw do
  get "home/index"
  get "home/minor"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: 'companies#index'
  
  resources :companies do
    #get 'search', on: :collection
    collection do
      get 'search'
      post 'import'
      get 'export'
      post 'export'
    end
    resources :contacts
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
