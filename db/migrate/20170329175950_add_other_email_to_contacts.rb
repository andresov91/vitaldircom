class AddOtherEmailToContacts < ActiveRecord::Migration[5.0]
  def change
    add_column :contacts, :other_email, :string
  end
end
