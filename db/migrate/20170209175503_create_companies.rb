class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :ruc
      t.text :name
      t.string :short_name
      t.string :trade_name
      t.text :address
      t.string :email
      t.string :website
      t.date :foundation
      t.belongs_to :country, index: true

      t.timestamps
    end

    create_join_table :companies, :activity_types do |t|
      t.index [:company_id, :activity_type_id], unique: true, name: 'index_act_types_companies_on_company_id_and_act_type_id'
    end

    create_join_table :companies, :product_categories do |t|
      t.index [:company_id, :product_category_id], unique: true, name: 'index_pr_categories_companies_on_company_id_and_pr_category_id'
    end
  end
end