class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :position
      t.string :phone
      t.string :cellphone
      t.string :email
      t.references :company, foreign_key: true

      t.timestamps
    end

    create_join_table :contacts, :contact_types do |t|
      t.index [:contact_id, :contact_type_id], unique: true, name: 'index_contacts_contact_types_on_contact_id_and_contact_type_id'
    end
  end
end
