# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Country.create(alpha2: 'ec', name: 'Ecuador')
Country.create(alpha2: 'co', name: 'Colombia')
Country.create(alpha2: 'pe', name: 'Peru')

ActivityType.create(name: 'Importador')
ActivityType.create(name: 'Exportador')
ActivityType.create(name: 'Distribuidor')
ActivityType.create(name: 'Representante')

ProductCategory.create(name: 'Papel')
ProductCategory.create(name: 'Acero')
ProductCategory.create(name: 'Plastico')
ProductCategory.create(name: 'Cacao')
ProductCategory.create(name: 'Camaron')

ContactType.create(name: 'Reportes')
ContactType.create(name: 'Bases')
ContactType.create(name: 'Suscripcion')
ContactType.create(name: 'Cobranzas')