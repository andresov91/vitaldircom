# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170329175950) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activity_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "activity_types_companies", id: false, force: :cascade do |t|
    t.integer "company_id",       null: false
    t.integer "activity_type_id", null: false
    t.index ["company_id", "activity_type_id"], name: "index_act_types_companies_on_company_id_and_act_type_id", unique: true, using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "ruc"
    t.text     "name"
    t.string   "short_name"
    t.string   "trade_name"
    t.text     "address"
    t.string   "email"
    t.string   "website"
    t.date     "foundation"
    t.integer  "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_companies_on_country_id", using: :btree
  end

  create_table "companies_product_categories", id: false, force: :cascade do |t|
    t.integer "company_id",          null: false
    t.integer "product_category_id", null: false
    t.index ["company_id", "product_category_id"], name: "index_pr_categories_companies_on_company_id_and_pr_category_id", unique: true, using: :btree
  end

  create_table "contact_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_types_contacts", id: false, force: :cascade do |t|
    t.integer "contact_id",      null: false
    t.integer "contact_type_id", null: false
    t.index ["contact_id", "contact_type_id"], name: "index_contacts_contact_types_on_contact_id_and_contact_type_id", unique: true, using: :btree
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "position"
    t.string   "phone"
    t.string   "cellphone"
    t.string   "email"
    t.integer  "company_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "other_email"
    t.index ["company_id"], name: "index_contacts_on_company_id", using: :btree
  end

  create_table "countries", force: :cascade do |t|
    t.string   "alpha2"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "contacts", "companies"
end
